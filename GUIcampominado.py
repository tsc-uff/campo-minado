import tkinter as tk
from tkinter import *
from cell import Cell
import random

root = Tk()


class Application():
    def __init__(self):
        self.root = root
        self.tela()
        self.frames_da_tela()
        self.criando_botoes()
        self.criando_label_entry()
        root.mainloop()
      

    def tela(self):
        self.root.title("Campo Minado")
        self.root.configure(background="#393E40")
        self.root.geometry("700x500")
        self.root.resizable(True, True)
        self.root.maxsize(width=900, height=700)
        self.root.minsize(width=400, height=300)


    def frames_da_tela(self):

        self.frame_1 = Frame(self.root, bd=4, bg="#949699",
                              highlightthickness=3)
        self.frame_1.place(relx=0.20, rely=0.08, relwidth=0.60, relheight=0.10)


        self.frame_2 = Frame(self.root, bd=4, bg="#949699",
                              highlightthickness=3)
        self.frame_2.place(relx=0.20, rely=0.20, relwidth=0.60, relheight=0.70)

    
    def criando_botoes(self):

        self.bt_iniciar = Button(self.root, text="Iniciar Jogo", command=self.iniciar_jogo)
        self.bt_iniciar.place(relx=0.40, rely=0.92, relwidth=0.20, relheight=0.06)

        self.bt_dica = Button(self.root, text="Dica")
        self.bt_dica.place(relx=0.20, rely=0.92, relwidth=0.12, relheight=0.06)

        self.bt_reset = Button(self.root, text="Reset")
        self.bt_reset.place(relx=0.68, rely=0.92, relwidth=0.12, relheight=0.06)

        self.bt_sair = Button(self.root, text="Sair",command=self.sair_jogo)
        self.bt_sair.place(relx=0.83, rely=0.92, relwidth=0.12, relheight=0.06)


    def criando_label_entry(self):
        self.lb_tamanho = Label(self.frame_1, text="Tamanho")
        self.lb_tamanho.place(relx=0.08, rely=0.25)

        self.tamanho_entry = Entry(self.frame_1)
        self.tamanho_entry.place(relx=0.30, rely=0.25, relwidth=0.12)


    def iniciar_jogo(self):
        self.tamanho = self.tamanho_entry.get()
        for x in range(int(self.tamanho)):
            for y in range(int(self.tamanho)):
                c = Cell(x, y)
                c.create_btn_object(self.frame_2)
                c.cell_btn_object.grid(column=x, row=y)

        Application.randomize_mines(self)
        Application.create_cell_count_label(self)


    def randomize_mines(self):

        picked_cells = random.sample(Cell.all, int(self.tamanho))
        for picked_cell in picked_cells:
            picked_cell.is_mine = True

    
    def create_cell_count_label(self):
        
        lbl = Label(root, text=f'Qtdade de Minas: {self.tamanho}', width=20, height=2, bg='black', fg='white', font=('', 8))
        lbl.place(x = 1, y = 200)
    
    def sair_jogo(self):
        root.destroy()

Application()
