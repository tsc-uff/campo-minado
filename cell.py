from tkinter import Button
import random
import ctypes

class Cell:
    all = []
    def __init__(self, x, y, is_mine=False):
        self.is_mine = is_mine
        self.cell_btn_object = None
        self.x = x
        self.y = y

        # Append the object to the Cell.all list
        Cell.all.append(self)

    def create_btn_object(self, location):
        btn = Button(location, width=4, height=1)

        btn.bind('<Button-1>', self.left_click_actions)
        btn.bind('<Button-3>', self.right_click_actions)

        self.cell_btn_object = btn

    def left_click_actions(self, event):
        if self.is_mine:
            self.show_mine()
        else:
            if self.surrounded_cells_mines_length == 0:
                for cell_object in self.surrounded_cells:
                    cell_object.show_cell()
            self.show_cell()

    def get_cell_by_axis(self, x, y):
        # Return a cell object based on the value of x, y
        for cell in Cell.all:
            if cell.x == x and cell.y == y:
                return cell


    @property
    def surrounded_cells(self):
        cells = [self.get_cell_by_axis(self.x - 1, self.y -1),
                           self.get_cell_by_axis(self.x - 1, self.y),
                           self.get_cell_by_axis(self.x - 1, self.y + 1),
                           self.get_cell_by_axis(self.x, self.y - 1),
                           self.get_cell_by_axis(self.x + 1, self.y - 1),
                           self.get_cell_by_axis(self.x + 1, self.y),
                           self.get_cell_by_axis(self.x + 1, self.y + 1),
                           self.get_cell_by_axis(self.x, self.y + 1),
                           ]
        
        cells = [cell for cell in cells if cell is not None]
        return cells 


    @property
    def surrounded_cells_mines_length(self):
        counter = 0
        for cell in self.surrounded_cells:
            if cell.is_mine:
                counter += 1
        return counter


    def show_cell(self):
        self.cell_btn_object.configure(text=self.surrounded_cells_mines_length)


    def show_mine(self):
        # A logic to interrupt the game and display a message that player lost
        self.cell_btn_object.configure(bg='red')
        ctypes.windll.user32.MessageBoxW(0, 'Você clicou em uma mina', 'Game Over', 0)

    def right_click_actions(self, event):
        pass

    def __repr__(self):
        return f'Cell({self.x}, {self.y})'